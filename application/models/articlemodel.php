<?php
/**
 * Created by PhpStorm.
 * User: Shan
 * Date: 12/15/2016
 * Time: 11:33 AM
 */

class ArticleModel extends CI_Model {

    public function articles_list()
    {

        $user_id = $this->session->userdata("user_id");
        $parameters = array("uid"=>$user_id);

        $query = $this->db->where($parameters)->get("articles");
        $result = $query->result();
        return $result;
    }
    public function insert_article($data)
    {
        $query = $this->db->insert('articles',$data);
        return $query;
    }

    public function fetch_article($id)
    {
        $parameters = array("id"=>$id);
        $query = $this->db->where($parameters)->get("articles");
        return $query->row();
    }

    public function update_article($data,$article_id)
    {
        $id = $article_id;
        $parameters = array("id"=>$id);
        $query = $this->db->where($parameters)->update('articles',$data);
        return $query;
    }

    public function delete_article($article_id)
    {
        $parameters = array("id"=>$article_id);
        $query = $this->db->where($parameters)->delete('articles');
        return $query;
    }

} 