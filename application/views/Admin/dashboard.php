<?php include('admin_header.php');?>

<div class="container">

    <div class="row">
        <div class="col-lg-6 col-lg-offset-6">
            <a href="<?php echo base_url("Admin/add_article")?>" class="btn  btn-lg btn-primary pull-right">Add Article</a>
        </div>
    </div>
    <table class="table">
        <thead>
            <th>Sr.No</th>
            <th>Title</th>
            <th>Action</th>
        </thead>
        <tbody>
        <?php
            if(count($list)):
                foreach ($list as $article):
                    ?>
                    <tr>
                        <td><?php echo $article->id ?></td>
                        <td><?php echo $article->title ?></td>

                        <td>
                            <div class="col-lg-2">
                                <form method="post" action="edit_article">
                                    <input type="hidden" name="edit_id" value=" <?php echo $article->id ?>">
                                    <button type="submit" class="btn btn-primary">Edit</button>

                                </form>
                            </div>

                            <div class="col-lg-2">
                                <form method="post" action="delete_article">
                                    <input type="hidden" name="delete_id" value=" <?php echo $article->id ?>">
                                    <button type="submit" class="btn btn-danger">Delete</button>

                                </form>
                            </div>



                        </td>

                    </tr
          <?php  endforeach; ?>
          <?php else: ?>
                    <tr>
                            <td>No Record Found</td>
                    </tr>
         <?php endif; ?>
        </tbody>
    </table>
</div>
<?php include('admin_footer.php');?>