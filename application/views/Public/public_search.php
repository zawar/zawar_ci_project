<?php include('public_header.php');?>

    <div class="container">

        <h1>Search Articles</h1>
        <hr>
            <table class="table">
                <thead>
                <th>Sr.No</th>
                <th>Title</th>
                <th>Published On</th>
                </thead>
                <tbody>
                <?php
                if(count($list)):
                    $count = $this->uri->segment(4,0);
                    foreach ($list as $article):
                        ?>
                        <tr>
                            <td><?php echo ++$count ?></td>
                            <td><?php echo $article->title ?></td>

                            <td>
                                <p>Data</p>
                            </td>

                        </tr
          <?php  endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td>No Record Found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        <?= $this->pagination->create_links(); ?>
    </div>
<?php include('public_footer.php');?>